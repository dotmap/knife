import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import '../lib/variables.scss'

export const HELLO_WORLD = gql`
  query ($name: String) {
    hello(name: $name)
  }
`

export const HelloWorld = ({ name }) => (
  <Query query={HELLO_WORLD} variables={{ name }}>
    {({ loading, error, data }) => {
      if (loading) return <p>not done yet</p>
      if (error) return <p>{JSON.stringify(error)}</p>

      return (
        <section className='hero is-primary'>
          <div className='hero-body'>
            <div className='container'>
              <h1 className='title'>{data.hello}</h1>
            </div>
          </div>
        </section>
      )
    }}
  </Query>
)
