import gql from 'graphql-tag'

const typeDef = gql`
  type Query {
    hello(name: String): String!
  }
`

const resolvers = {
  Query: {
    hello: (_, { name }) => `Hello ${name || 'World'}`
  }
}

export { typeDef, resolvers }
